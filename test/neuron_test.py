import unittest
from ann.neuron import Neuron
from ann.neurons.input_neuron import InputNeuron
from ann.connection import Connection
from ann.functions.sigmoid import Sigmoid

class TestNeuron(unittest.TestCase):

    def test_no_function_set(self):
        neuron = Neuron()
        with self.assertRaises(ValueError):
            neuron.output

    def test_output_zero(self):
        neuron = Neuron(activation=Sigmoid())

        self.assertEqual(neuron.output, 0.5)
        
    def test_add_neuron(self):
        n1 = Neuron(activation=Sigmoid())
        n2 = Neuron(activation=Sigmoid())

        self.assertEqual(n1 + n2, 1)

    def test_add_inputs(self):        
        n2 = Neuron(activation=Sigmoid())
        n3 = Neuron(activation=Sigmoid())
        c1 = Connection(neuron=n2)
        c2 = Connection(neuron=n3)

        n1 = Neuron(activation=Sigmoid())
        n1.add_connection(c1).add_connection(c2)

        self.assertNotEqual(n1.output, 0)

    def test_activation(self):
        input = .2
        weight = .5
        function = Sigmoid()
        n = Neuron(activation=function)
        i = InputNeuron(input)
        c = Connection(i, weight)
        n.add_connection(c)

        self.assertEqual(n.net_input, input * weight)
        self.assertEqual(n.output, function.calculate(input * weight))
        self.assertAlmostEqual(function.derivate(input), function.calculate(input) * (1 - function.calculate(input)))