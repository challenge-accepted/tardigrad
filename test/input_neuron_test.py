import unittest
from ann.neurons.input_neuron import InputNeuron

class TestInputNeuron(unittest.TestCase):

    def test_output(self):
        n1 = InputNeuron(.4)

        self.assertEqual(n1.output, .4)

    def test_neuron_add(self):
        n1 = InputNeuron(1)
        n2 = InputNeuron(.3)

        self.assertEqual(n1 + n2, 1.3)