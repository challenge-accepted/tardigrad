
import unittest
from ann.trainers.rbm import RBM
from ann.networks.multi_layer import MultiLayerNetwork
from ann.dataset import Dataset

class TestRBM(unittest.TestCase):
    
    def test_rbm_constructor(self):
        network = MultiLayerNetwork(2, 3, 2)

        rbm = RBM(network.layers[1]).rbm

        rbm[0].set_inputs([0, 1])
        firstOutput = rbm[1].output

        rbm[0].set_inputs([1, 1])
        secondOutput = rbm[1].output

        self.assertNotEqual(firstOutput, secondOutput)

    def test_reverse_constructor(self):
        network = MultiLayerNetwork(2, 3, 2)

        reverse = RBM(network.layers[1]).reverse

        reverse[0].set_inputs([0, 1, 0])
        firstOutput = reverse[1].output

        reverse[0].set_inputs([1, 1, 1])
        secondOutput = reverse[1].output

        self.assertNotEqual(firstOutput, secondOutput)

    def test_update_weight(self):
        network = MultiLayerNetwork(2, 3, 2)
        input = [1, 0]
        
        output = network.fit(input).output

        rbm = RBM(network.layers[1])

        dataset = Dataset()
        dataset.add_inputs(input)

        rbm.epoch(dataset, iterations=20).update_weights().update_layer()

        newOutput = network.fit(input).output
        
        self.assertNotEqual(output, newOutput)

    def test_reconstruction(self):
        network = MultiLayerNetwork(2, 3, 2)
        input = [1, 0]

        rbm = RBM(network.layers[1])

        dataset = Dataset()
        dataset.add_inputs(input)

        rbm.epoch(dataset, iterations=20).update_weights()
        firstError = rbm.error(input)
        
        rbm.epoch(dataset, iterations=20)
        lastError = rbm.error(input)
        
        self.assertLess(lastError, firstError)
