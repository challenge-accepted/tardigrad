import unittest
from ann.trainers.backpropagation import BackPropagation
from ann.networks.multi_layer import MultiLayerNetwork

class TestBackPropagation(unittest.TestCase):

    def setUp(self):
        self.network = MultiLayerNetwork(2, 3, 2)
        self.trainer = BackPropagation(self.network)

    def test_error_calculus(self):
        self.trainer.train([.2, .3], [.5, .8]).update_weights()

        self.assertNotEqual(self.trainer.globalError([.5, .8]), [0, 0])

    def test_error_descending(self):
        input = [.2, 1]
        labels = [.5, .8]

        startError = self.network.error(labels)

        for index in range(1, 100):
            self.trainer.train(input, labels).update_weights()

        error = self.network.error(labels)

        self.assertLess(error, startError)