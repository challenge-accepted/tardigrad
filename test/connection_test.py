import unittest
from ann.connection import Connection
from ann.neuron import Neuron
from ann.neurons.input_neuron import InputNeuron 
from ann.functions.sigmoid import Sigmoid

class TestConnection(unittest.TestCase):

    def test_add(self):
        n1 = Neuron(activation=Sigmoid())
        c1 = Connection(neuron=n1, weight=1)
        c2 = Connection(neuron=n1, weight=1)
        
        self.assertEqual(c1 + c2, 1)

    def test_with_bias(self):
        n1 = Neuron(activation=Sigmoid())
        n2 = InputNeuron(1)
        c1 = Connection(neuron=n1, weight=1)
        c2 = Connection(neuron=n2, weight=1)
        
        self.assertEqual(c1 + c2, 1.5)

    def test_create_connection(self):
        conn = Connection.create_connection(activation=Sigmoid(), weight=1)

        self.assertIsNotNone(conn.neuron)
        self.assertIsNotNone(conn.weight)
    