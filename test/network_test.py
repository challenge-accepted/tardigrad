import unittest
from ann.layer import Layer
from ann.network import Network
from ann.functions.sigmoid import Sigmoid

class TestNetwork(unittest.TestCase):

    def setUp(self):
        self.network = Network(inputSize=2)

    def test_input_layer(self):
        inputLayer = self.network.layers[0]
        for neuron in inputLayer.neurons:
            neuron.set_input(.2)

        self.assertEqual(inputLayer.neurons[0].output, .2)
        self.assertEqual(inputLayer.neurons[1].output, .2)

    def test_layer_connection(self):
        inLayer = self.network.layers[0]
        outLayer = Layer.create_layer(2, activation=Sigmoid())
        self.network.add_layer(outLayer)

        self.assertEqual(len(outLayer.neurons), 2)
        for neuron in outLayer.neurons:
            self.assertEqual(len(neuron.inputs), 2)
            for index, connection in enumerate(neuron.inputs):
                self.assertEqual(connection.neuron, inLayer.neurons[index])

    def test_not_enough_layers(self):
        with self.assertRaises(Exception):
            self.network.fit([])

    def test_output(self):
        outLayer = Layer.create_layer(2, activation=Sigmoid())
        self.network.add_layer(outLayer)

        startOutput = self.network.output

        output = self.network.fit([.2, .3]).output

        self.assertNotEqual(startOutput, output)

    def test_output_with_hidden(self):
        hidLayer = Layer.create_layer(2, activation=Sigmoid())
        outLayer = Layer.create_layer(2, activation=Sigmoid())
        self.network.add_layer(hidLayer).add_layer(outLayer)

        self.network.fit([.2, .3])

        hidOutput = self.network.layer_output(1)
        output = self.network.output

        self.assertNotEqual(hidOutput, output)

    def test_loop_connection(self):
        outLayer = Layer.create_layer(2, activation=Sigmoid())

        self.network.add_layer(outLayer)

        with self.assertRaises(ValueError):
            self.network.add_layer(outLayer)

    def test_save_and_load(self):
        hidLayer = Layer.create_layer(2, activation=Sigmoid())
        outLayer = Layer.create_layer(3, activation=Sigmoid())
        self.network.add_layer(hidLayer).add_layer(outLayer)
        
        self.network.set_bias(1)
        self.network.set_bias(2)

        self.network.fit([.2, .3])
        hidOutput = self.network.layer_output(1)
        output = self.network.output

        self.network.save('network.yml')

        # load and test output
        self.network.load('network.yml')
        self.network.save('_network.yml')

        self.network.fit([.2, .3])
        newHidOutput = self.network.layer_output(1)
        newOutput = self.network.output

        self.assertEqual(hidOutput, newHidOutput)
        self.assertEqual(output, newOutput)
