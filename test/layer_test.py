import unittest
from ann.layer import Layer
from ann.neurons.input_neuron import InputNeuron
from ann.functions.sigmoid import Sigmoid

class TestLayer(unittest.TestCase):

    def test_layer_create(self):
        l2 = Layer.create_layer(2, Sigmoid())

        self.assertEqual(l2.length, 2)

    def test_layer_create_input(self):
        l2 = Layer.create_input_layer(2)

        self.assertEqual(l2.length, 2)
        self.assertIsInstance(l2.neurons[0], InputNeuron)

    def test_layer_activation(self):
        l2 = Layer.create_layer(2, Sigmoid())

        self.assertIsInstance(l2.activation, Sigmoid)

    def test_iterator(self):
        l2 = Layer.create_input_layer(2)
        neurons = l2.iterator()

        self.assertIsInstance(next(neurons), InputNeuron)
        self.assertIsInstance(next(neurons), InputNeuron)
        with self.assertRaises(StopIteration):
            next(neurons)

    def test_bias(self):
        pass