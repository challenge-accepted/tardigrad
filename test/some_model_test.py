import unittest
from ann.networks.multi_layer import MultiLayerNetwork
from ann.trainers.backpropagation import BackPropagation
from ann.dataset import Dataset

class TestModels(unittest.TestCase):
    
    def __like(self, value1, value2):
        limit = 0.1
        like = True

        for index, v1 in enumerate(value1):
            if abs(v1 - value2[index]) >= limit:
                like = False

        return like

    def test_training(self):
        network = MultiLayerNetwork(3, 2, 2)
        trainer = BackPropagation(network)

        input = [.2, .6, 1]
        labels = [0, 1]

        network.set_bias(1)

        csv = open('training.csv', 'w')

        for index in range(1, 5000):
            trainer.train(input, labels).update_weights()

            line = "%i,%f\n" % (index, network.error(labels))
            csv.write(line)

            if network.error(labels) < 0.001: break

        csv.close()

        output = network.fit(input).output
        
        self.assertTrue(self.__like(labels, output))

    def test_xor(self):
        network = MultiLayerNetwork(2, 3, 1)
        trainer = BackPropagation(network, learningRate=0.3, momentum=0.8)

        network.set_bias(1).set_bias(2)
        
        dataset = Dataset()
        dataset.add_from_file('data/xor.csv', inputSize=2)

        csv = open('training.csv', 'w')     
        index = 0   
        for index in range(5000):
            trainer.epoch(dataset)

            line = "%i,%f\n" % (index, network.fit([1,1]).error([0]))
            csv.write(line)

            if network.fit([1, 1]).error([0]) < .001: break

        csv.close()
        
        self.assertTrue(self.__like(network.output, [0]))

    @unittest.skip
    def test_online_vs_offline_training(self):
        
        dataset = Dataset()
        dataset.add_from_file('data/xor.csv', inputSize=2)
        
        def train_full_epoch():
            network = MultiLayerNetwork(2, 3, 1)
            network.set_bias(1).set_bias(2)
            trainer = BackPropagation(network, learningRate=0.3, momentum=0.8)

            index = 0
            for index in range(5000):
                trainer.epoch(dataset)

                index += 1
                if network.fit([1, 1]).error([0]) < .001: break

            return index

        def train_each_epoch():
            network = MultiLayerNetwork(2, 3, 1)
            network.set_bias(1).set_bias(2)
            trainer = BackPropagation(network, learningRate=0.3, momentum=0.8)

            index = 0
            for index in range(5000):
                trainer.epoch(dataset, online=False)
                
                index += 1
                if network.fit([1, 1]).error([0]) < .001: break

            return index

        # compare data training methods
        fullTimes = []
        eachTimes = []

        for i in range(10):
            fullTimes.append(train_full_epoch())
            eachTimes.append(train_each_epoch())

        #print( 'Full epoch training: ', sum(fullTimes)/len(fullTimes), 'Each item training: ', sum(eachTimes)/len(eachTimes) )

        self.assertLess(sum(fullTimes)/len(fullTimes), sum(eachTimes)/len(eachTimes))