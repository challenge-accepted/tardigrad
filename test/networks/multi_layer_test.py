import unittest
from ann.networks.multi_layer import MultiLayerNetwork
from ann.neurons.input_neuron import InputNeuron
from ann.neuron import Neuron

class MLNTest(unittest.TestCase):

    def test_creation(self):
        mln = MultiLayerNetwork(2, 3, 2)

        self.assertEqual(len(mln.layers), 3)

    def test_creation_not_enough_layers(self):
        with self.assertRaises(Exception):
            mln = MultiLayerNetwork(2)

    def test_output(self):
        mln = MultiLayerNetwork(2, 3, 2)

        self.assertNotEqual(mln.output, 0)

    def test_layer_creation(self):
        mln = MultiLayerNetwork(2, 3, 2)

        self.assertEqual(len(mln.layers), 3)

    def test_connectins(self):
        mln = MultiLayerNetwork(2, 3, 2)

        self.assertEqual(len(mln.layers), 3)

        for neuron in mln.layers[0].neurons:
            self.assertIsInstance(neuron, InputNeuron)

        for neuron in mln.layers[1].neurons:
            self.assertIsInstance(neuron, Neuron)
            self.assertEqual(len(neuron.inputs), 2)

        for neuron in mln.layers[2].neurons:
            self.assertIsInstance(neuron, Neuron)
            self.assertEqual(len(neuron.inputs), 3)

    def test_output(self):
        mln = MultiLayerNetwork(2, 2)
        out = mln.layers[1]

        mln.fit([.2, .8])

        out1 = out.neurons[0].inputs[0].weight * out.neurons[0].inputs[0].neuron.output
        out1 += out.neurons[0].inputs[1].weight * out.neurons[0].inputs[1].neuron.output
        out1 = out.neurons[0].activation.calculate(out1)

        out2 = out.neurons[1].inputs[0].weight * out.neurons[1].inputs[0].neuron.output
        out2 += out.neurons[1].inputs[1].weight * out.neurons[1].inputs[1].neuron.output
        out2 = out.neurons[1].activation.calculate(out2)

        self.assertEqual([out1, out2], mln.output)

    def test_structure(self):
        mln = MultiLayerNetwork(2, 2, 1)
        
        self.assertEqual(len(mln.layers), 3)

        for lIndex in range(1, len(mln.layers)):
            layer = mln.layers[lIndex]
            bLayer = mln.layers[lIndex - 1]

            for nIndex, neuron in enumerate(layer.neurons):
                for cIndex, connection in enumerate(neuron.inputs):
                    self.assertIn(connection.neuron, bLayer.neurons)        
