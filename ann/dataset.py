import csv

class Dataset:

    def __init__(self):
        self.__samples = []

    def add_inputs(self, *inputs):
        """Creates a new sample from inputs and returns its instance"""

        sample = Sample(inputs[0] if type(inputs[0]) is list else inputs)
        self.__samples.append(sample)

        return sample

    def add_from_file(self, path, inputSize):
        """Creates a dataset from a csv file, given its input size"""

        inputs = None

        with open(path, 'r') as file:
            inputs = csv.reader(file)

            for row in inputs:
                self.add_inputs([float(item) for item in row[:inputSize]]).set_outputs([float(item) for item in row[inputSize:]])
        
        return self

    @property
    def samples(self):
        return self.__samples

    @property
    def length(self):
        return len(self.__samples)

class Sample:

    def __init__(self, inputs=[]):
        self.__sample = {
            'inputs': inputs,
            'outputs': []
        }

    def set_outputs(self, *outputs):
        self.__sample['outputs'] = outputs[0] if type(outputs[0]) is list else outputs

    @property
    def sample(self):
        return self.__sample

    @property
    def inputs(self):
        return self.__sample['inputs']

    @property
    def outputs(self):
        return self.__sample['outputs']