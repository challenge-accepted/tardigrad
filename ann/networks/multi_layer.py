from ann.network import Network
from ann.layer import Layer
from ann.functions.sigmoid import Sigmoid

class MultiLayerNetwork(Network):

    def __init__(self, *layerSizes):
        """Creates a multi layer network with the given amount of elements of each layer"""

        if len(layerSizes) < 2:
            raise Exception('You should specify at least two layers to create a network')

        super().__init__(layerSizes[0])

        for index, layerSize in enumerate(layerSizes):
            if index == 0: continue

            layer = Layer.create_layer(layerSize, Sigmoid())

            super().add_layer(layer)