import random
from ann.function import Function
from ann.neuron import Neuron

class Connection:

    def __init__(self, neuron=None, weight=random.uniform(0, 1)):
        self.neuron = neuron
        self.weight = weight

    @property
    def output(self):
        return self.neuron.output * self.weight

    @staticmethod
    def create_connection(activation: Function, weight=None):
        neuron = Neuron(activation=activation)
        connection = Connection(neuron=neuron, weight=weight)

        return connection
    
    #operators
    def __add__(self, other):
        return self.output + other.output