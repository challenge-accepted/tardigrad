from ann.neurons.input_neuron import InputNeuron

class BiasNeuron(InputNeuron):
    """
        An input neuron is used to create fixed bias connections for other neurons
    """

    def __init__(self, input=1):
        super().__init__(input=input)