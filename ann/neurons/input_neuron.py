from ann.neuron import Neuron

class InputNeuron(Neuron):
    """
        An input neuron is used to create the input layer on the networks 
    """

    def __init__(self, input=0):
        super().__init__()

        self.__input = input

    def set_input(self, input):
        """Sets a fixed value as output for this neuron"""

        self.__input = input

    @property
    def output(self):
        return self.__input