from ann.neuron import Neuron
from ann.neurons.input_neuron import InputNeuron
from ann.neurons.bias_neuron import BiasNeuron
from ann.connection import Connection
import random

class Layer:

    def __init__(self):
        self.__neurons = []

    @property
    def neurons(self):
        return self.__neurons

    @property
    def length(self):
        return len(self.__neurons)

    @property
    def output(self):
        output = []
        for neuron in self.neurons:
            output.append(neuron.output)

        return output

    def iterator(self):
        for neuron in self.__neurons:
            yield neuron

    def add_neuron(self, neuron):
        self.__neurons.append(neuron)

        return self

    def set_bias(self, value):
        biasNeuron = BiasNeuron(value)

        for neuron in self.__neurons:
            biasConn = Connection(biasNeuron, weight=random.uniform(-.5, .5))
            neuron.add_connection(biasConn)

    def set_activation(self, activation):
        for neuron in self.__neurons:
            if neuron is not InputNeuron:
                neuron.activation = activation

    def set_inputs(self, inputs):
        for index, neuron in enumerate(self.__neurons):
            if isinstance(neuron, InputNeuron):
                neuron.set_input(inputs[index])

    @staticmethod
    def create_layer(size:0, activation):
        """Creates an layer with given size of neurons running a given activation function"""

        layer = Layer()
        layer.activation = activation

        for neuron in [Neuron(activation=activation) for index in range(size)]:
            layer.add_neuron(neuron)

        return layer

    @staticmethod
    def create_input_layer(size:0):
        """Creates an layer with given size of input neurons"""

        layer = Layer()
        for neuron in [InputNeuron() for index in range(size)]:
            layer.add_neuron(neuron)

        return layer

