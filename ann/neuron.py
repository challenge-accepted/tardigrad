
class Neuron:

    def __init__(self, activation=None):
        # input connections to sum up and process
        self.__inputs = []
        self.__activation = activation

    @property
    def activation(self):
        return self.__activation

    @activation.setter
    def activation(self, function):
        self.__activation = function

    @property
    def inputs(self):
        return self.__inputs

    @property
    def output(self):
        """Calculates the neuron output given its inputs connections"""

        if(self.__activation is None):
            raise ValueError('No activation function defined yet!')
        
        input = self.net_input

        return self.__activation.calculate(input)

    @property
    def net_input(self):
        """Calculates the neuron output given its inputs connections"""
        
        input = 0
        if len(self.__inputs) > 0:
            input = sum([input + i.output for i in self.__inputs])

        return input

    def add_connection(self, connection):
        self.__inputs.append(connection)

        return self

    
    #operators
    def __add__(self, other):
        return self.output + other.output;

    