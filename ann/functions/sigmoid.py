from ann.function import Function
import math

class Sigmoid(Function):
    name = 'sigmoid'
    
    def calculate(self, value):
        return 1 / (1 + math.exp(-1 * value))

    def derivate(self, value):
        return math.exp(value) / (1 + math.exp(value))**2
