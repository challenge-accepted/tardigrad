from ann.function import Function
import math

class Tanh(Function):
    name = 'tanh'
    
    def calculate(self, value):
        return 2 / (1 + math.exp(-2 * value)) - 1

    def derivate(self, value):
        x = self.calculate(value)
        
        return 1 - x**2
