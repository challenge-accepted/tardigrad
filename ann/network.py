from ann.layer import Layer
from ann.connection import Connection
from ann.neurons.bias_neuron import BiasNeuron
from ann.neuron import Neuron
from ann.functions.sigmoid import Sigmoid
from ann.functions.tanh import Tanh
import yaml

class Network:

    def __init__(self, inputSize=None):
        self.__layers = []

        if inputSize is not None:
            inLayer = Layer.create_input_layer(inputSize)
            self.add_layer(inLayer)

    @property
    def layers(self):
        """Returns the layers of this network"""

        return self.__layers

    @layers.setter
    def layers(self, layers):
        self.__layers = layers

        return self

    def add_layer(self, layer:Layer):
        """Adds a layer to this network"""

        if len(self.__layers) > 0:
            lastLayer = self.__layers[len(self.__layers) - 1]

            if lastLayer == layer:
                raise ValueError('You are trying to connect a layer in a loop.')
                
            self.__connectLayers(lastLayer, layer)

        self.__layers.append(layer)

        return self

    def set_bias(self, layerIndex, value=1):
        self.layers[layerIndex].set_bias(value)

        return self

    def set_activation(self, layerIndex, activation):
        self.layers[layerIndex].set_activation(activation)

        return self

    def error(self, labels):
        sum = 0

        for index, output in enumerate(self.output):
            sum += ( 1/2 * (labels[index] - output)**2 )
        
        return sum

    @property
    def output(self):
        """Calculates the output of the network after evaluation"""
        return self.layer_output()

    def layer_output(self, layerIndex=None):
        """Calculates the layer output of the network after evaluation"""

        # last layer as default 
        if layerIndex is None:
            layerIndex = len(self.__layers) - 1

        output = self.__layers[layerIndex].output

        return output

    def fit(self, inputs):
        """Evaluates the given float input array"""
        
        if len(self.__layers) < 2:
            raise Exception('Not enough layers to evaluate output!')

        inputLayer = self.__layers[0]

        # set network input
        for input, value in zip(inputLayer.neurons, inputs):
            input.set_input(value)

        return self

    def save(self, path):
        """Save this network to a file on a given path"""

        network = {
            'input': 0,
            'layers': {}
        }

        for lIndex, layer in enumerate(self.__layers):
            if lIndex == 0: 
                network['input'] = len(layer.neurons)
                continue

            network['layers'][lIndex] = {}
            for nIndex, neuron in enumerate(layer.neurons):
                network['layers'][lIndex][nIndex] = {
                    'activation': neuron.activation.name,
                    'inputs': {}
                }
                for cIndex, connection in enumerate(neuron.inputs):
                    if isinstance(connection.neuron, BiasNeuron):
                        network['layers'][lIndex][nIndex]['bias'] = {
                            'weight': connection.weight,
                            'value': connection.neuron.output
                        }
                    else:
                        network['layers'][lIndex][nIndex]['inputs'][cIndex] = connection.weight
                    
        with open(path, 'w') as file:
            yaml.dump(network, file, default_flow_style=False)
            file.close()

    def load(self, path):
        """Load a network from a file from the given path"""
        
        network = None

        with open(path, 'r') as file:
            network = yaml.load(file)
            file.close()

        self.__layers = []

        # add input Layer
        newLayer = Layer.create_input_layer(network['input'])
        self.__layers.append(newLayer)

        # create and connect layers from saved network
        lastLayer = newLayer
        for lIndex in network['layers']:
            inputLayer = network['layers'][lIndex]

            newLayer = Layer()

            # just used if some neurons in the layer has a layer connection
            layerBias = BiasNeuron()

            for nIndex in inputLayer:
                neuron = Neuron()
                activation = inputLayer[nIndex]['activation']
                
                if(activation == 'sigmoid'):
                    neuron.activation = Sigmoid()
                elif(activation == 'tanh'):
                    neuron.activation = Tanh()
                else:
                    # default activation function
                    neuron.activation = Sigmoid()

                # create input connections for neuron
                for cIndex, iNeuron in enumerate(lastLayer.neurons):
                    weight = inputLayer[nIndex]['inputs'][cIndex]
                    connection = Connection(neuron=iNeuron, weight=weight)
                    
                    neuron.add_connection(connection)

                if 'bias' in inputLayer[nIndex]:
                    bias = inputLayer[nIndex]['bias']
                    layerBias.set_input(bias['value'])
                    connection = Connection(neuron=layerBias, weight=bias['weight'])

                    neuron.add_connection(connection)

                newLayer.add_neuron(neuron)

            self.__layers.append(newLayer)
            lastLayer = newLayer
            

    # private methods

    def __connectLayers(self, inputs:Layer, outputs:Layer):
        for outNeuron in outputs.neurons:
            for inNeuron in inputs.neurons:
                outNeuron.add_connection(Connection(neuron=inNeuron))