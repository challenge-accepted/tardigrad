from ann.trainer import Trainer
from ann.neurons.input_neuron import InputNeuron
import time
import copy

class BackPropagation(Trainer):

    def __init__(self, network, learningRate = .5, momentum=0):
        self.__network = network
        self.__learningRate = learningRate
        self.__momentum = momentum
        self.__lastIteration = {}
        self.__lastEpoch = {}

    def globalError(self, labels):
        errors = []

        for index, output in enumerate(self.__network.output):
            errors.append(output - labels[index])
        
        return errors

    def epoch(self, dataset, online=True):
        if(online):
            for sample in dataset.samples:
                self.train(sample.inputs, sample.outputs)

            self.update_weights(dataset.length)
        else:
            for sample in dataset.samples:
                self.train(sample.inputs, sample.outputs).update_weights()

        return self

    def train(self, inputs, labels):
        self.__network.fit(inputs)

        globalError = self.globalError(labels)

        # reverse layer list
        rLayers = list(reversed(self.__network.layers))

        # run back propagation and adjust weights
        totalErrors = []
        for layerIndex in range(0, len(rLayers) - 1):
            layer = rLayers[layerIndex]

            if layerIndex > 0:
                globalError = totalErrors[:]
                totalErrors = []
            else:
                totalErrors = []

            # correct connections
            for index, neuron in enumerate(layer.neurons):
                netInput = neuron.net_input
                
                for connIndex, connection in enumerate(neuron.inputs):
                    # calculate the total error in respect to the connection
                    deltaIRTNeuron = globalError[index] * neuron.activation.derivate(netInput)
                    totalErrorIRTConnection = deltaIRTNeuron * connection.neuron.output

                    # add iteration values
                    self.last_iteration(layerIndex, index, connIndex, totalErrorIRTConnection)
                    
                    # propagate the errors to layer bellow but not for bias connection
                    if connection.neuron is not InputNeuron:
                        deltaIRTNeuron *= connection.weight
                        if index == 0:
                            totalErrors.append(deltaIRTNeuron)
                        else:
                            totalErrors[connIndex] += deltaIRTNeuron
        
        return self

    def update_weights(self, epoch_size=1):
        """Update epoch data and saves the last epoch for momentum calculations"""

        rLayers = list(reversed(self.__network.layers))
        for layerIndex in range(0, len(rLayers) - 1):
            layer = rLayers[layerIndex]
            
            for index, neuron in enumerate(layer.neurons):
                for connIndex, connection in enumerate(neuron.inputs):
                    connection.weight -= self.__learningRate * self.last_iteration(layerIndex, index, connIndex) / epoch_size + \
                        self.__momentum * self.last_epoch(layerIndex, index, connIndex)

        self.__lastEpoch = copy.deepcopy(self.__lastIteration)
        self.__lastIteration = {}

        return self

    def last_iteration(self, layer, neuron, connection, value=None):
        """Used to sum up the iterations in this epoch"""

        if value is None:
            if layer in self.__lastIteration.keys():
                if neuron in self.__lastIteration[layer].keys():
                    if connection in self.__lastIteration[layer][neuron].keys():
                        return self.__lastIteration[layer][neuron][connection]

            return 0
        else:
            if layer not in self.__lastIteration.keys(): self.__lastIteration[layer] = {}
            if neuron not in self.__lastIteration[layer].keys(): self.__lastIteration[layer][neuron] = {}
            if connection not in self.__lastIteration[layer][neuron].keys(): self.__lastIteration[layer][neuron][connection] = 0

            self.__lastIteration[layer][neuron][connection] += value

    def last_epoch(self, layer, neuron, connection):
        """Used to keep the last epoch for momentum updates"""

        if layer in self.__lastEpoch.keys():
            if neuron in self.__lastEpoch[layer].keys():
                if connection in self.__lastEpoch[layer][neuron].keys():
                    return self.__lastEpoch[layer][neuron][connection]
        else:
            return 0