
from ann.neurons.bias_neuron import BiasNeuron
from ann.neurons.input_neuron import InputNeuron
from ann.connection import Connection
from ann.neuron import Neuron
from ann.layer import Layer
import copy

class RBM():
    
    def __init__(self, hiddenLayer, learningRate=0.2):
        self.__learningRate = learningRate
        self.__originalLayer = hiddenLayer
        self.__rbm = None
        self.__reverse = None

        # accumulators
        self.__sisjPos = None
        self.__sisjNeg = None
        self.__cd = None

        self.__reconstruction = []

        # create rbm structure from given hidden layer
        self.__create_rbm(copy.deepcopy(hiddenLayer))

    @property
    def rbm(self):
        return self.__rbm

    @property
    def reverse(self):
        return self.__reverse

    @property
    def reconstruction(self):
        return self.__reverseOuputs

    def __initialize(self):
        self.__reconstruction = []

        for input in range(len(self.__sisjPos)):
            for output in range(len(self.__sisjPos[input])):
                self.__sisjPos[input][output] = 0
                self.__sisjNeg[input][output] = 0
                self.__cd[input][output] = 0

    def epoch(self, dataset, iterations = 1):
        self.__initialize()

        # for each data input in dataset
        for sample in dataset.samples:
            sisj0 = self.__iterate(sample.inputs)

            for n in range(iterations):
                sisjn = self.__iterate()
            
            # update accumulated sisj
            for input in range(len(self.__sisjPos)):
                for output in range(len(self.__sisjPos[input])):
                    self.__sisjPos[input][output] += sisj0[0][input] * sisj0[1][output]
                    self.__sisjNeg[input][output] += sisjn[0][input] * sisjn[1][output]

        # compute contrastive divergence
        for input in range(len(self.__sisjPos)):
            for output in range(len(self.__sisjPos[input])):
                self.__cd[input][output] += self.__sisjPos[input][output] / dataset.length - self.__sisjNeg[input][output] / dataset.length
        
        return self

    def update_layer(self):
        """
            Update network weights from RBM
            Should be called after update_weights to update this RBM
        """

        for nIndex, neuron in enumerate(self.__rbm[1].neurons):
            for cIndex, connection in enumerate(neuron.inputs):
                self.__originalLayer.neurons[nIndex].inputs[cIndex].weight += self.__learningRate * self.__cd[cIndex][nIndex]

        return self

    def update_weights(self):
        """Update RBM weights"""
        
        # Updates RBM weights
        for nIndex, neuron in enumerate(self.__rbm[1].neurons):
            for cIndex, connection in enumerate(neuron.inputs):
                connection.weight += self.__learningRate * self.__cd[cIndex][nIndex]

        # Updates reverse RBM weights
        for nIndex, neuron in enumerate(self.__reverse[1].neurons):
            for cIndex, connection in enumerate(neuron.inputs):
                connection.weight += self.__learningRate * self.__cd[nIndex][cIndex]

        return self

    def error(self, inputs):
        error = 0
        for index, output in enumerate(self.__reconstruction):
            error += 1/2 * (output - inputs[index])**2

        return error

    def __iterate(self, inputs=None):
        inputData = inputs if inputs else self.__reconstruction

        self.__rbm[0].set_inputs(inputData)
        output = self.__rbm[1].output

        self.__reverse[0].set_inputs(output)
        self.__reconstruction = self.__reverse[1].output

        # return si and sj for the iteration
        return [inputData, output]

    def __create_rbm(self, originalLayer):
        rbmInputLayer = Layer()
        reverseInputLayer = Layer()
        reverseOutputLayer = Layer()

        # create input layer for the rbm using hidden layer input connection length
        # and the reverse output layer for reverse rbm
        baseNeuron = originalLayer.neurons[0]
        for connection in baseNeuron.inputs:
            if isinstance(connection.neuron, BiasNeuron):
                continue

            rbmInputLayer.add_neuron(InputNeuron())
            reverseOutputLayer.add_neuron(Neuron(activation=baseNeuron.activation))
       
        for nIndex, neuron in enumerate(originalLayer.neurons):
            # add neuron to reverse rbm input layer
            reverseInputLayer.add_neuron(InputNeuron())

            # update layer connections to point to input layer
            for index, connection in enumerate(neuron.inputs):
                if isinstance(connection.neuron, BiasNeuron):
                    continue

                connection.neuron = rbmInputLayer.neurons[index]

                reverseConnection = Connection(neuron=reverseInputLayer.neurons[nIndex], weight=connection.weight)
                reverseOutputLayer.neurons[index].add_connection(reverseConnection)

        self.__rbm = [rbmInputLayer, originalLayer]
        self.__reverse = [reverseInputLayer, reverseOutputLayer]

        # initializing acumulation matrices
        self.__sisjPos = self.__sisjNeg = self.__cd = [[0 for input in range(len(originalLayer.neurons))] for output in range(len(rbmInputLayer.neurons))]
