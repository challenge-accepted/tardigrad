
from ann.trainer import Trainer
from ann.trainers.rbm import RBM 

class DBN(Trainer):

    def __init__(self, network):
        self.network = network

    def epoch(self, dataset, online=True):
        pass

    def train(self, inputLayerIndex, inputs):
        RBMInput = inputs

        if inputLayerIndex > 0:
            # forward step
            self.network.fit(inputs)

            # get RBM input for the actual machine
            RBMInput = self.network.layer_output(inputLayerIndex)

        newLayer = RBM(self.network.layers[inputLayerIndex + 1]).train(RBMInput)

        # update layer with new weights
        self.network.layers[inputLayerIndex + 1] = newLayer
        