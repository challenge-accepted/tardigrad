# Tardigrade

Tardigrade is a Deep Learning library, written in Python 3.x, 
used to create neural networks and train them with backpropagation, using
labeled sample data, or RBM, for non-labeled sample data.

## Setup Environment

```
[sudo] pip3 install -r requirements.txt
```

## Test

```
./test.sh
```